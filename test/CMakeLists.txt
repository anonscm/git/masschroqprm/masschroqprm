# CMake script for PAPPSOms++ library
# Author: Olivier Langella
# Created: 03/03/2015 from the CMakeLists file of the Bio++ project

FIND_PACKAGE( Qt4 REQUIRED )

SET (QT_USE_QTXML true)
INCLUDE( ${QT_USE_FILE} )

configure_file (config.h.cmake ${CMAKE_SOURCE_DIR}/test/config.h)

#apt-get install libpwiz-dev libboost-dev
set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake_modules)
FIND_PACKAGE(Pwiz)
IF (Pwiz_FOUND)
  INCLUDE_DIRECTORIES( ${Pwiz_INCLUDE_DIR})
  FIND_PACKAGE(Boost REQUIRED)
  INCLUDE_DIRECTORIES( ${Boost_INCLUDE_DIRS})
ELSE (Pwiz_FOUND)
  MESSAGE("Proteowizard library not found")
  MESSAGE("did you apt-get install libpwiz-dev libboost-dev ?")
ENDIF (Pwiz_FOUND)
#CTEST_OUTPUT_ON_FAILURE=TRUE make test
#make test ARGS="-V"

#export LD_LIBRARY_PATH=/home/langella/developpement/git/pappsomspp/build/src:$LD_LIBRARY_PATH

MACRO(TEST_FIND_LIBRARY OUTPUT_LIBS lib_name include_to_find)
  #start:
  FIND_PATH(${lib_name}_INCLUDE_DIR ${include_to_find})

  SET(${lib_name}_NAMES ${lib_name} ${lib_name}.lib ${lib_name}.dll)
  FIND_LIBRARY(${lib_name}_LIBRARY NAMES ${${lib_name}_NAMES})
  IF(${lib_name}_LIBRARY)
    MESSAGE("-- Library ${lib_name} found here:")
    MESSAGE("   includes: ${${lib_name}_INCLUDE_DIR}")
    MESSAGE("   dynamic libraries: ${${lib_name}_LIBRARY}")
    MESSAGE(WARNING "Library ${lib_name} is already installed in the system tree. Test will be built against it. This may lead to unexpected results. You may want to do 'make install' before 'make test', or remove the installed version.")
  ELSE()
    MESSAGE("${lib_name}_LIBRARY -L../src -lpappsomspp")
    MESSAGE("${lib_name}_INCLUDE_DIR ../src/")
    MESSAGE("${LIBS}")
    SET(${lib_name}_LIBRARY "-L../src -lpappsomspp")
    SET(${lib_name}_INCLUDE_DIR "../src/")
  ENDIF()
  INCLUDE_DIRECTORIES(${${lib_name}_INCLUDE_DIR})
  SET(${OUTPUT_LIBS} ${${OUTPUT_LIBS}} ${${lib_name}_LIBRARY})
ENDMACRO(TEST_FIND_LIBRARY)

#Find the bpp-core library library:
TEST_FIND_LIBRARY(LIBS pappsomspp pappsomspp/types.h pappsomspp/peptide/peptidenaturalisotopelist.h)


# File list
SET(CPP_TEST_FILES
  saxparsers/xtandemresultshandler.cpp
  )
  
  
ADD_EXECUTABLE(test_massrange test_massrange.cpp)
TARGET_LINK_LIBRARIES(test_massrange ${LIBS}   ${QT_LIBRARIES})
ADD_TEST(test_massrange "test_massrange")

ADD_EXECUTABLE(test_aa test_aa.cpp)
TARGET_LINK_LIBRARIES(test_aa ${LIBS}   ${QT_LIBRARIES})
ADD_TEST(test_aa "test_aa")

ADD_EXECUTABLE(test_peptide test_peptide.cpp)
TARGET_LINK_LIBRARIES(test_peptide ${LIBS}   ${QT_LIBRARIES})
ADD_TEST(test_peptide "test_peptide")

ADD_EXECUTABLE(test_peptidenaturalisotopelist test_peptidenaturalisotopelist.cpp)
TARGET_LINK_LIBRARIES(test_peptidenaturalisotopelist ${LIBS}   ${QT_LIBRARIES})
ADD_TEST(test_peptidenaturalisotopelist "test_peptidenaturalisotopelist")

ADD_EXECUTABLE(test_peptidefragment test_peptidefragment.cpp)
TARGET_LINK_LIBRARIES(test_peptidefragment ${LIBS}   ${QT_LIBRARIES})
ADD_TEST(test_peptidefragment "test_peptidefragment")

ADD_EXECUTABLE(test_fragmentationcid test_fragmentationcid.cpp)
TARGET_LINK_LIBRARIES(test_fragmentationcid ${LIBS}   ${QT_LIBRARIES})
ADD_TEST(test_fragmentationcid "test_fragmentationcid")

IF (FALSE) 
ADD_EXECUTABLE(test_big_hyperscore test_big_hyperscore.cpp ${CPP_TEST_FILES})
TARGET_LINK_LIBRARIES(test_big_hyperscore ${LIBS} ${QT_LIBRARIES})
ADD_TEST(test_big_hyperscore "test_big_hyperscore")
SET(TEST_LIST ${TEST_LIST} test_big_hyperscore)
ENDIF(FALSE)

IF (Pwiz_FOUND)

  ADD_EXECUTABLE(test_isotope_with_spectrum test_isotope_with_spectrum.cpp)
  TARGET_LINK_LIBRARIES(test_isotope_with_spectrum ${LIBS}   ${QT_LIBRARIES} ${Pwiz_LIBRARY} ${Boost_LIBRARIES})
  ADD_TEST(test_isotope_with_spectrum "test_isotope_with_spectrum")
  SET(TEST_LIST ${TEST_LIST} test_isotope_with_spectrum)
  
  ADD_EXECUTABLE(test_hyperscore test_hyperscore.cpp)
  TARGET_LINK_LIBRARIES(test_hyperscore ${LIBS}   ${QT_LIBRARIES} ${Pwiz_LIBRARY} ${Boost_LIBRARIES})
  ADD_TEST(test_hyperscore "test_hyperscore")
  SET(TEST_LIST ${TEST_LIST} test_hyperscore)

  ADD_EXECUTABLE(test_xtandem_spectrum test_xtandem_spectrum.cpp)
  TARGET_LINK_LIBRARIES(test_xtandem_spectrum ${LIBS}   ${QT_LIBRARIES} ${Pwiz_LIBRARY} ${Boost_LIBRARIES})
  ADD_TEST(test_xtandem_spectrum "test_xtandem_spectrum")
  SET(TEST_LIST ${TEST_LIST} test_xtandem_spectrum)

    ADD_EXECUTABLE(test_scan_15968 test_scan_15968.cpp)
  TARGET_LINK_LIBRARIES(test_scan_15968 ${LIBS}   ${QT_LIBRARIES} ${Pwiz_LIBRARY} ${Boost_LIBRARIES})
  ADD_TEST(test_scan_15968 "test_scan_15968")
  SET(TEST_LIST ${TEST_LIST} test_scan_15968)

ENDIF(Pwiz_FOUND)

IF(UNIX)
  SET_PROPERTY(TEST test_massrange test_aa test_peptide test_peptidenaturalisotopelist test_peptidefragment test_fragmentationcid ${TEST_LIST} PROPERTY ENVIRONMENT "LD_LIBRARY_PATH=$ENV{LD_LIBRARY_PATH}:../src")
ENDIF()

IF(WIN32)
  SET(ENV{PATH} "$ENV{PATH};..\\src")
ENDIF()

