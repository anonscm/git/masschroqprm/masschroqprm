Source: masschroqprm
Maintainer: Olivier Langella <olivier.langella@u-psud.fr>
Homepage: http://pappso.inra.fr/bioinfo
Section: science
Priority: optional
Build-Depends: debhelper (>= 9),
               cmake (>= 2.6),
               qt5-default,
               libpappsomspp-dev (= @LIBPAPPSOMSPP_VERSION@),
               libodsstream-dev
Standards-Version: 3.9.8

Package: masschroqprm
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libpappsomspp0 (=@LIBPAPPSOMSPP_VERSION@),
         libodsstream0
Pre-Depends: ${misc:Pre-Depends}
Description: mass chromatogram quantification using MS/MS transitions
 (Parallel Reaction Monitoring).
