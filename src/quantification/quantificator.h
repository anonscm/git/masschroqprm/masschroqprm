
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of MassChroqPRM.
 *
 *     MassChroqPRM is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroqPRM is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <pappsomspp/msrun/msrunid.h>
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include <QDate>
#include <QUrl>
#include <odsstream/calcwriterinterface.h>

#include "../peptides/peptidelist.h"
#include "prmxic.h"


class QuantificatorParam
{
  public:
  bool _quantifyIsotope = true;
  pappso::PrecisionPtr _precursor_ion_mass_tolerance =
    PrecisionFactory::getDaltonInstance(1.5);
  pappso::PrecisionPtr _fragment_ion_mass_tolerance =
    PrecisionFactory::getDaltonInstance(0.02);
};


class Quantificator
{
  public:
  Quantificator(const MsRunId &msrun_id,
                const PeptideList &peptideList,
                const QuantificatorParam &param);
  ~Quantificator();
  void addMsmsSpectrum(const pappso::MassSpectrum &spectrum,
                       pappso_double precMz,
                       pappso_double retentionTime);
  void addQualifiedSpectrum(const pappso::QualifiedMassSpectrum &qspectrum);
  void debriefing() const;
  void report(CalcWriterInterface &writer) const;
  void reportGeneralIons(CalcWriterInterface &writer) const;
  const MsRunId &
  getMsRunId() const
  {
    return _msrun_id;
  };

  private:
  const MsRunId _msrun_id;
  QuantificatorParam _param;

  std::list<NoConstPrmXicSp> _prmXicList;

  pappso::pappso_double _mass_range_min = 200;
  pappso::pappso_double _mass_range_max = 1000;
};
