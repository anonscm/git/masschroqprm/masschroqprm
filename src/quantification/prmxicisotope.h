/*
 * /*******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 * ******************************************************************************/

#ifndef PRMXICISOTOPE_H
#define PRMXICISOTOPE_H

#include "prmxic.h"
#include <pappsomspp/psm/peakionisotopematch.h>
#include <pappsomspp/xic/xicpeptidefragmentionnaturalisotope.h>

using namespace pappso;

class PrmXicIsotope: public PrmXic
{
public:
    PrmXicIsotope(const MsRunId & msrun_id, const PrmPeptideSp & peptideSp, unsigned int z, PrecisionPtr precision);
    PrmXicIsotope (const PrmXicIsotope & other) ;
    ~PrmXicIsotope();
    virtual NoConstPrmXicSp makeNoConstPrmXicSp() const override;

    virtual void push_back(const pappso::MassSpectrum & spectrum, pappso_double retentionTime) override;


    virtual void print(QTextStream & out) const override;
    virtual void report(CalcWriterInterface & writer) const override;


    virtual void reportGeneralLine(CalcWriterInterface & writer) const override;

private :
  
    XicPeptideFragmentIonNaturalIsotope & findXic(const pappso::PeakIonIsotopeMatch& peptide_ion_isotope_match);


    std::vector<PeptideNaturalIsotopeAverageSp> _v_peptideIsotopeList;
    std::vector<PeptideFragmentIonSp> _v_peptideIonList;


    std::vector<XicPeptideFragmentIonNaturalIsotope> _xicIsotopeList;


};

#endif // PRMXICISOTOPE_H
