/*
 * /*******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 * ******************************************************************************/


#include "prmxicisotope.h"
#include <pappsomspp/psm/peptideisotopespectrummatch.h>
#include <pappsomspp/peptide/peptidenaturalisotopelist.h>
#include <pappsomspp/exception/exceptionoutofrange.h>

//#include "../utils/consoleout.h"

PrmXicIsotope::PrmXicIsotope(const MsRunId & msrun_id, const PrmPeptideSp & peptideSp, unsigned int z, PrecisionPtr precision)
    :PrmXic(msrun_id, peptideSp, z, precision)
{
    qDebug() << "PrmXicIsotope::PrmXicIsotope begin";
    unsigned int parent_charge = z;
    unsigned int maxIsotopeLevel = 1;

    std::map<const PeptideFragmentIon*, PeptideNaturalIsotopeListSp> mapIonIsotopeList;

    for (unsigned int charge=1; charge <= parent_charge; charge++) {
        for (auto && ionSp : _p_peptide_fragment_ion_list_sp.get()->getPeptideFragmentIonList()) {
            const PeptideNaturalIsotopeList * p_isotopeList;
            auto mapIonIsotopeListIt = mapIonIsotopeList.find(ionSp.get());
            if (mapIonIsotopeListIt == mapIonIsotopeList.end()) {
                PeptideNaturalIsotopeList isotopeList(ionSp);
                qDebug() << "PrmXicIsotope::PrmXicIsotope isotopeList.size() " << isotopeList.size();
                PeptideNaturalIsotopeListSp isotopeListSp = isotopeList.makePeptideNaturalIsotopeListSp();
                qDebug() << "PrmXicIsotope::PrmXicIsotope p_isotopeList->size() " << isotopeListSp.get()->size();
                mapIonIsotopeList.insert(std::pair<const PeptideFragmentIon*, PeptideNaturalIsotopeListSp>(ionSp.get(),isotopeListSp));

                p_isotopeList = isotopeListSp.get();
            }
            else {
                qDebug() << "PrmXicIsotope::PrmXicIsotope 2";
                p_isotopeList = mapIonIsotopeListIt->second.get();
            }

            qDebug() << "PrmXicIsotope::PrmXicIsotope 3";
            qDebug() << "PrmXicIsotope::PrmXicIsotope 4 " << p_isotopeList->size();
            unsigned int askedIsotopeRank = 1;
            for (unsigned int isotopeLevel=0; isotopeLevel <= maxIsotopeLevel; isotopeLevel++) {
                PeptideNaturalIsotopeAverage isotopeIon(*p_isotopeList, askedIsotopeRank, isotopeLevel, charge, _precision);
                _v_peptideIsotopeList.push_back(isotopeIon.makePeptideNaturalIsotopeAverageSp());
                _v_peptideIonList.push_back(ionSp);
            }
        }
    }


}


PrmXicIsotope::PrmXicIsotope (const PrmXicIsotope & other) 
:PrmXic(other), _v_peptideIsotopeList(other._v_peptideIsotopeList), _v_peptideIonList(other._v_peptideIonList), _xicIsotopeList(other._xicIsotopeList) {
  qDebug() << "PrmXicIsotope::PrmXicIsotope (const PrmXicIsotope & other) " ;
}

NoConstPrmXicSp PrmXicIsotope::makeNoConstPrmXicSp() const {
    return std::make_shared<PrmXicIsotope>(*this);
}



XicPeptideFragmentIonNaturalIsotope & PrmXicIsotope::findXic(const pappso::PeakIonIsotopeMatch& peptide_ion_isotope_match) {
    qDebug() << "PrmXicIsotope::findXic begin ";
    const PeptideNaturalIsotopeAverageSp  isotopeSp= peptide_ion_isotope_match.getPeptideNaturalIsotopeAverageSp();

    auto it = _xicIsotopeList.begin();
    while (it != _xicIsotopeList.end()) {
        if (it->getPeptideNaturalIsotopeAverageSp() == isotopeSp) {
            return *it;
        }
        it++;
    }
    _xicIsotopeList.push_back(XicPeptideFragmentIonNaturalIsotope(_msrun_id, isotopeSp, peptide_ion_isotope_match.getPeptideFragmentIonSp()));
    qDebug() << "PrmXicIsotope::findXic end ";
    return (_xicIsotopeList.back());

}

void PrmXicIsotope::push_back(const pappso::MassSpectrum & spectrum, pappso_double retentionTime) {
    qDebug() << "PrmXicIsotope::push_back spectrum " << _peptideSp.get()->getSequence() << " mz="<< getMz();

    PeptideIsotopeSpectrumMatch psm (spectrum, _v_peptideIsotopeList, _v_peptideIonList, _precision);

    if (psm.size() >= 4) {
        pappso_double tic = 0;
        auto peptideIonMatchIt = psm.begin();
        while (peptideIonMatchIt != psm.end()) {
            tic+= peptideIonMatchIt->getPeak().y;

            const XicSPtr & xic = findXic(*peptideIonMatchIt).getXicSPtr();
            xic.get()->push_back(DataPoint(retentionTime, peptideIonMatchIt->getPeak().y));

            peptideIonMatchIt++;
        }

        _xic_tic.push_back(DataPoint(retentionTime, tic));

        // qDebug() << "PrmXic::push_back " << psm.size();
    }
    //computeHyperscore(spectrum);
}


PrmXicIsotope::~PrmXicIsotope()
{

}





void PrmXicIsotope::print(QTextStream & out) const {

    out << _peptideSp.get()->getXmlId()  << " z=" <<_z<< endl;
    for (auto && xicPeak :_xic_tic) {
        out << xicPeak.x << "\t" << xicPeak.y << endl;
    }
    out.flush();
}


void PrmXicIsotope::reportGeneralLine(CalcWriterInterface & writer) const {
    // qDebug() << "PrmXic::reportGeneralLine begin";
    writer.writeLine();
    //writer.writeCell("peptide id");
    writer.writeCell(_msrun_id.getXmlId());
    writer.writeCell(_peptideSp.get()->getXmlId());

    // qDebug() << "PrmXic::reportGeneralLine 1";
    // writer.writeCell("charge");
    writer.writeCell((int) _z);
    // writer.writeCell("sequence");
    writer.writeCell(_peptideSp.get()->getSequence());
    // writer.writeCell("mass");
    writer.writeCell(_peptideSp.get()->getMass());

    // qDebug() << "PrmXic::reportGeneralLine 2";
    //writer.writeCell("m/z");
    writer.writeCell(_peptideSp.get()->getMz(_z));
    //  qDebug() << "PrmXic::reportGeneralLine 22";
    //writer.writeCell("formula");
    writer.writeCell(_peptideSp.get()->getFormula(_z));

    // qDebug() << "PrmXic::reportGeneralLine 3";
    writer.writeCell(this->computeTicArea());

    try {
        writer.writeCell(_max_peak_tic.getXicPeak().getArea());
    }
    catch (const MassChroqPrmException & e) {
        qDebug() << e.qwhat();
        writer.writeEmptyCell();
    }

    // qDebug() << "PrmXic::reportGeneralLine end";
}

void PrmXicIsotope::report(CalcWriterInterface & writer) const {
    qDebug() << "PrmXicIsotope::report begin";
    if (_xicIsotopeList.size() > 0) {
        writer.writeSheet(QString("%1_%2-%3").arg(_msrun_id.getXmlId()).arg(_peptideSp.get()->getXmlId()).arg(QString().setNum(_z)));
        //writer.writeSheet(QString(_peptideSp.get()->getXmlId()).append("-").append(QString().setNum(_z)));
        writer.writeLine();
        writer.writeCell("rt");
        writer.writeCell("intensity (sum of matches peaks)");
        for (auto && xic : _xicIsotopeList) {
            const PeptideNaturalIsotopeAverage & isotope = *(xic.getPeptideNaturalIsotopeAverageSp().get());
            writer.writeCell(QString("%1-%2+%3-%4").arg(xic.getPeptideFragmentIonSp().get()->getName()).arg(xic.getCharge()).arg(isotope.getIsotopeNumber()).arg(isotope.getIsotopeRank()));
        }
        for (auto && xicPeak :_xic_tic) {
            writer.writeLine();
            writer.writeCell(xicPeak.x);
            writer.writeCell(xicPeak.y);
            for (auto && xic : _xicIsotopeList) {
                try {
                    const DataPoint & peak = xic.getXicCstSPtr().get()->at(xicPeak.x);
                    writer.writeCell(peak.y);
                }
                catch (const pappso::ExceptionOutOfRange& oor) {
                    //qDebug() << "PrmXic::report oor " << oor.qwhat();
                    writer.writeEmptyCell();
                }
            }
        }
    }

    // qDebug() << "PrmXic::report end";
}
