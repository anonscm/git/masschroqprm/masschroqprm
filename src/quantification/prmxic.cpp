
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of MassChroqPRM.
 *
 *     MassChroqPRM is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroqPRM is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "prmxic.h"
#include <pappsomspp/peptide/peptidefragmention.h>
#include <pappsomspp/psm/peptidespectrummatch.h>
#include <pappsomspp/peptide/peptidefragmention.h>
#include <pappsomspp/psm/xtandem/xtandemhyperscore.h>
#include <pappsomspp/psm/xtandem/xtandemspectrumprocess.h>
#include <pappsomspp/peptide/peptidefragmentionlistbase.h>
#include <pappsomspp/exception/exceptionoutofrange.h>
#include <pappsomspp/exception/exceptionnotpossible.h>
#include <cmath>

//#include "../utils/consoleout.h"


PrmXic::PrmXic(const MsRunId &msrun_id,
               const PrmPeptideSp &peptideSp,
               unsigned int z,
               PrecisionPtr precision)
  : _msrun_id(msrun_id), _peptideSp(peptideSp), _z(z)
{

  _precision = precision;
  _ionListToMonitor.push_back(pappso::PeptideIon::y);
  _ionListToMonitor.push_back(pappso::PeptideIon::b);
  _p_peptide_fragment_ion_list_sp =
    PeptideFragmentIonListBase(peptideSp, _ionListToMonitor)
      .makePeptideFragmentIonListBaseSp();
}


PrmXic::PrmXic(const PrmXic &other)
  : _msrun_id(other._msrun_id),
    _peptideSp(other._peptideSp),
    _z(other._z),
    _xicList(other._xicList)
{
  qDebug() << "PrmXic::PrmXic (const PrmXic & other)";
  _ionListToMonitor               = other._ionListToMonitor;
  _p_peptide_fragment_ion_list_sp = other._p_peptide_fragment_ion_list_sp;
  _xic_tic                        = other._xic_tic;
  _max_peak_tic                   = other._max_peak_tic;
  _precision                      = other._precision;
}

PrmXic::~PrmXic()
{
}

pappso::pappso_double
PrmXic::getMz() const
{
  return _peptideSp.get()->getMz(_z);
}

NoConstPrmXicSp
PrmXic::makeNoConstPrmXicSp() const
{
  return std::make_shared<PrmXic>(*this);
}

XicPeptideFragmentIon &
PrmXic::findXic(const pappso::PeakIonMatch &peptide_ion_match)
{

  PeptideFragmentIonSp fragIonSp = peptide_ion_match.getPeptideFragmentIonSp();
  unsigned int charge            = peptide_ion_match.getCharge();

  auto &&it = _xicList.begin();
  while(it != _xicList.end())
    {
      // compares the stored pointers
      if((it->getPeptideFragmentIonSp() == fragIonSp) &&
         (it->getCharge() == charge))
        {
          return *it;
        }
      it++;
    }


  _xicList.push_back(
    XicPeptideFragmentIon(_msrun_id, fragIonSp, charge, _precision));

  return (_xicList.back());
}

void
PrmXic::push_back(const pappso::MassSpectrum &spectrum,
                  pappso_double retentionTime)
{
  qDebug() << "PrmXic::push_back spectrum begin "
           << _peptideSp.get()->getSequence() << " mz=" << getMz();

  PeptideSpectrumMatch psm(spectrum,
                           *(_p_peptide_fragment_ion_list_sp.get()),
                           _z,
                           _precision,
                           _ionListToMonitor);
  qDebug() << "PrmXic::push_back spectrum " << psm.size();

  if(psm.size() >= 4)
    {
      pappso_double tic = 0;
      for(auto &&peptide_ion_match : psm)
        {
          tic += peptide_ion_match.getPeak().y;

          XicSPtr xic = findXic(peptide_ion_match).getXicSPtr();
          xic.get()->push_back(
            DataPoint(retentionTime, peptide_ion_match.getPeak().y));
        }

      _xic_tic.push_back(DataPoint(retentionTime, tic));

      // qDebug() << "PrmXic::push_back " << psm.size();
    }
  // computeHyperscore(spectrum);
}


/// anciens rt
pappso_double
PrmXic::computeTicArea() const
{

  pappso_double matchedPeakArea    = 0;
  pappso_double previous_rtime     = 0;
  pappso_double previous_intensity = 0;
  for(auto &&peak : _xic_tic)
    {
      if(previous_rtime != 0)
        {
          matchedPeakArea += ((fabs((double)peak.x - previous_rtime)) *
                              (peak.y + previous_intensity)) /
                             2;
        }

      previous_rtime     = peak.x;
      previous_intensity = peak.y;
    }
  return matchedPeakArea;
}


void
PrmXic::computeHyperscore(const pappso::MassSpectrum &spectrumIn)
{

  PrecisionPtr precision = PrecisionFactory::getDaltonInstance(0.02);
  std::list<pappso::PeptideIon> ion_list;
  ion_list.push_back(pappso::PeptideIon::y);
  ion_list.push_back(pappso::PeptideIon::b);

  XtandemSpectrumProcess spectrum_process;
  spectrum_process.setRemoveIsotope(true);
  spectrum_process.setDynamicRange(100);
  spectrum_process.setNmostIntense(100);
  spectrum_process.setMinimumMz(150);
  pappso::MassSpectrum spectrum =
    spectrum_process.process(spectrumIn, _peptideSp.get()->getMz(_z), _z);
  /*
  unsigned int charge_pep15968 = 2;
  Spectrum
  spectrum_parent(spectrum_isotopes.removeParent(pep15968.getPeptideSp(),
  charge_pep15968, 2, 2)); if (! spectrum_parent.equals(sremove_parent,
  precision)) { cerr << "removeParent() != tandem parent"<< endl; return 1;
  }
  */


  /*
  Spectrum
  spectrum_neutral(spectrum_drange.removeNeutral(pep15968.getPeptideSp(),
  charge_pep15968, MASSH2O, DaltonPrecision(0.5))); if (!
  spectrum_neutral.equals(sremove_neutral, precision)) { cerr <<
  "spectrum_neutral() != tandem"<< endl; return 1;
  }
  */
  bool refine_spectrum_synthesis = true;
  // spectrum.debugPrintValues();
  XtandemHyperscore hyperscore(
    spectrum, _peptideSp, _z, precision, ion_list, refine_spectrum_synthesis);

  qDebug() << " PrmXic::computeHyperscore " << hyperscore.getHyperscore()
           << " ";
}


void
PrmXic::print(QTextStream &out) const
{

  out << _peptideSp.get()->getXmlId() << " z=" << _z << endl;
  for(auto &&xicPeak : _xic_tic)
    {
      out << xicPeak.x << "\t" << xicPeak.y << endl;
    }
  out.flush();
}

void
PrmXic::reportGeneralHeader(CalcWriterInterface &writer)
{
  writer.writeLine();
  writer.writeCell("sample id");
  writer.writeCell("peptide id");
  writer.writeCell("charge");
  writer.writeCell("sequence");
  writer.writeCell("mass");
  writer.writeCell("m/z");
  writer.writeCell("formula");
  writer.writeCell("sum of intensity");
  writer.writeCell("sum of detected peak areas");
}

void
PrmXic::reportGeneralLine(CalcWriterInterface &writer) const
{
  qDebug() << "PrmXic::reportGeneralLine begin";
  writer.writeLine();
  // writer.writeCell("peptide id");
  writer.writeCell(_msrun_id.getXmlId());
  writer.writeCell(_peptideSp.get()->getXmlId());

  // qDebug() << "PrmXic::reportGeneralLine 1";
  // writer.writeCell("charge");
  writer.writeCell((int)_z);
  // writer.writeCell("sequence");
  writer.writeCell(_peptideSp.get()->getSequence());
  // writer.writeCell("mass");
  writer.writeCell(_peptideSp.get()->getMass());

  // qDebug() << "PrmXic::reportGeneralLine 2";
  // writer.writeCell("m/z");
  writer.writeCell(_peptideSp.get()->getMz(_z));
  //  qDebug() << "PrmXic::reportGeneralLine 22";
  // writer.writeCell("formula");
  // qDebug() << _peptideSp.get()->getFormula(_z);
  writer.writeCell(_peptideSp.get()->getFormula(_z));

  // qDebug() << "PrmXic::reportGeneralLine 3";
  writer.writeCell(this->computeTicArea());

  try
    {
      writer.writeCell(_max_peak_tic.getXicPeak().getArea());
    }
  catch(const MassChroqPrmException &e)
    {
      qDebug() << e.qwhat();
      writer.writeEmptyCell();
    }
  // qDebug() << "PrmXic::reportGeneralLine end";
}

void
PrmXic::report(CalcWriterInterface &writer) const
{
  qDebug() << "PrmXic::report begin";
  if(_xicList.size() > 0)
    {
      writer.writeSheet(QString("%1_%2-%3")
                          .arg(_msrun_id.getXmlId())
                          .arg(_peptideSp.get()->getXmlId())
                          .arg(QString().setNum(_z)));
      writer.writeLine();
      writer.writeCell("rt");
      writer.writeCell("intensity (sum of matches peaks)");
      for(auto &&xic : _xicList)
        {
          writer.writeCell(
            QString("%1-%2")
              .arg(xic.getPeptideFragmentIonSp().get()->getName())
              .arg(xic.getCharge()));
        }
      for(auto &&xicPeak : _xic_tic)
        {
          writer.writeLine();
          writer.writeCell(xicPeak.x);
          writer.writeCell(xicPeak.y);
          for(auto &&xic : _xicList)
            {
              try
                {
                  const DataPoint &peak =
                    xic.getXicCstSPtr().get()->at(xicPeak.x);
                  writer.writeCell(peak.y);
                }
              catch(const pappso::ExceptionOutOfRange &oor)
                {
                  // qDebug() << "PrmXic::report oor " << oor.qwhat();
                  writer.writeEmptyCell();
                }
            }
        }
    }

  // qDebug() << "PrmXic::report end";
}

void
PrmXic::detectXicTic()
{
  qDebug() << "PrmXic::detectXicTic begin " << _xic_tic.size();
  TraceDetectionZivy _zivy(2, 3, 3, 6000, 5000);

  try
    {
      _zivy.detect(_xic_tic, _max_peak_tic);
    }
  catch(const ExceptionNotPossible &e)
    {
      qDebug() << e.qwhat();
    }
}
