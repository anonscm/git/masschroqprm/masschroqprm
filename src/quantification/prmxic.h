
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of MassChroqPRM.
 *
 *     MassChroqPRM is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroqPRM is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <pappsomspp/msrun/msrunid.h>
#include <pappsomspp/peptide/peptide.h>
#include <pappsomspp/massspectrum/massspectrum.h>
#include <pappsomspp/peptide/peptidefragmentionlistbase.h>
#include <pappsomspp/xic/xicpeptidefragmention.h>
#include <pappsomspp/xic/xicpeptidefragmentionnaturalisotope.h>
#include <odsstream/calcwriterinterface.h>
#include <pappsomspp/processing/detection/tracedetectionzivy.h>

#include "../peptides/prmpeptide.h"
#include "../utils/masschroqprmexception.h"


using namespace pappso;

class PrmXic;

typedef std::shared_ptr<PrmXic> NoConstPrmXicSp;


class XicDetectionMaxSink : public TraceDetectionSinkInterface
{
  public:
  void
  setTracePeak(TracePeak &xic_peak) override
  {
    _count++;
    if(xic_peak.getArea() > _peak_max.getArea())
      {
        _peak_max = xic_peak;
      }
  };
  const TracePeak &
  getXicPeak() const
  {
    if(_count == 0)
      throw MassChroqPrmException(QObject::tr("no peak detected"));
    return _peak_max;
  };

  private:
  unsigned int _count = 0;
  TracePeak _peak_max;
};


class PrmXic
{
  public:
  PrmXic(const MsRunId &msrun_id,
         const PrmPeptideSp &peptideSp,
         unsigned int z,
         PrecisionPtr precision);
  PrmXic(const PrmXic &other);
  ~PrmXic();

  virtual NoConstPrmXicSp makeNoConstPrmXicSp() const;
  pappso::pappso_double getMz() const;
  virtual void push_back(const pappso::MassSpectrum &spectrum,
                         pappso_double retentionTime);

  virtual void print(QTextStream &out) const;
  virtual void report(CalcWriterInterface &writer) const;

  virtual void detectXicTic();


  static void reportGeneralHeader(CalcWriterInterface &swriter);
  virtual void reportGeneralLine(CalcWriterInterface &writer) const;

  protected:
  pappso_double computeTicArea() const;

  protected:
  const MsRunId _msrun_id;
  const PrmPeptideSp _peptideSp;
  const unsigned int _z;


  PeptideFragmentIonListBaseSp _p_peptide_fragment_ion_list_sp;
  Xic _xic_tic;
  XicDetectionMaxSink _max_peak_tic;

  std::list<pappso::PeptideIon> _ionListToMonitor;

  PrecisionPtr _precision = PrecisionFactory::getDaltonInstance(0.02);


  private:
  void computeHyperscore(const pappso::MassSpectrum &spectrum);
  XicPeptideFragmentIon &findXic(const pappso::PeakIonMatch &peptide_ion_match);


  private:
  std::vector<XicPeptideFragmentIon> _xicList;
};
