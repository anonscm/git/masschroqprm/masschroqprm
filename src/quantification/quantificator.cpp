
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of MassChroqPRM.
 *
 *     MassChroqPRM is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroqPRM is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "quantificator.h"

//#include "../utils/consoleout.h"
#include "prmxicisotope.h"

Quantificator::Quantificator(const MsRunId &msrun_id,
                             const PeptideList &peptideList,
                             const QuantificatorParam &param)
  : _msrun_id(msrun_id), _param(param)
{
  qDebug() << "Quantificator::Quantificator begin peptideList.size() "
           << peptideList.size();
  for(auto &&peptideSp : peptideList)
    {
      for(unsigned int z = 1; z < 6; z++)
        {
          qDebug() << "Quantificator::Quantificator peptideSp z "
                   << peptideSp.get()->getMz(z) << " " << z;
          pappso::pappso_double peptideMz = peptideSp.get()->getMz(z);
          if((peptideMz > _mass_range_min) && (peptideMz < _mass_range_max))
            {
              NoConstPrmXicSp xicSp;
              if(param._quantifyIsotope)
                {
                  xicSp = PrmXicIsotope(_msrun_id, peptideSp, z,
                                        param._fragment_ion_mass_tolerance)
                            .makeNoConstPrmXicSp();
                }
              else
                {
                  xicSp = PrmXic(_msrun_id, peptideSp, z,
                                 param._fragment_ion_mass_tolerance)
                            .makeNoConstPrmXicSp();
                }
              qDebug() << "Quantificator::Quantificator push_back prmXic "
                       << xicSp.get()->getMz();
              _prmXicList.push_back(xicSp);
            }
        }
    }
}

Quantificator::~Quantificator()
{
}

void
Quantificator::addQualifiedSpectrum(const pappso::QualifiedMassSpectrum &qspectrum)
{
  try
    {
      // void
      // Quantificator::addMsmsSpectrum(const pappso::Spectrum &spectrum, mz
      // precMz,
      //                               pappso_double retentionTime)
      //{
      qDebug() << "Quantificator::addMsmsSpectrum "
               << qspectrum.getPrecursorMz() << " "
               << qspectrum.getRtInSeconds();
      qDebug() << "Quantificator::addMsmsSpectrum _prmXicList.size()"
               << _prmXicList.size();

      pappso::MzRange massRange(qspectrum.getPrecursorMz(),
                                  _param._precursor_ion_mass_tolerance);
      for(auto &&prmXicSp : _prmXicList)
        {
          qDebug() << "Quantificator::addMsmsSpectrum prmXicSp"
                   << prmXicSp.get()->getMz();
          if(massRange.contains(prmXicSp.get()->getMz()))
            {
              prmXicSp.get()->push_back(*(qspectrum.getMassSpectrumCstSPtr().get()), qspectrum.getRtInSeconds());
              break;
            }
        }
      qDebug() << "Quantificator::addMsmsSpectrum end";
    }
  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("error in Quantificator::addMsmsSpectrum :\n")
          .append(error.qwhat()));
    }

  catch(std::exception &error)
    {
      throw pappso::PappsoException(
        QObject::tr("error in Quantificator::addMsmsSpectrum :\n")
          .append(error.what()));
    }
}


void
Quantificator::debriefing() const
{
  qDebug() << "Quantificator::debriefing begin";
  for(auto &&prmXicSp : _prmXicList)
    {
      prmXicSp.get()->detectXicTic();
      // prmXicSp.get()->print(mcqprmout());
    }
  qDebug() << "Quantificator::debriefing end";
}

void
Quantificator::reportGeneralIons(CalcWriterInterface &writer) const
{
  qDebug() << "Quantificator::reportGeneralFragments begin";
  for(auto &&prmXicSp : _prmXicList)
    {
      prmXicSp.get()->reportGeneralLine(writer);
    }

  qDebug() << "Quantificator::reportGeneralFragments end";
}

void
Quantificator::report(CalcWriterInterface &writer) const
{
  qDebug() << "Quantificator::report begin";
  for(auto &&prmXicSp : _prmXicList)
    {
      prmXicSp.get()->report(writer);
    }

  qDebug() << "Quantificator::report end";
}
