
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of MassChroqPRM.
 *
 *     MassChroqPRM is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroqPRM is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "masschroqprm_cli.h"

#include <QDebug>
#include <QCoreApplication>
#include <QTimer>
#include <QDateTime>
#include <QDir>
#include <QCommandLineParser>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/massspectrum/massspectrum.h>
#include <pappsomspp/pappsoexception.h>
#include <odsstream/odsdocwriter.h>
#include <odsstream/tsvdirectorywriter.h>
#include <vector>
#include "quantification/quantificator.h"


//#include <pwiz/data/msdata/MSDataFile.hpp>
#include "peptides/peptidelist.h"
#include "utils/masschroqprmexception.h"
#include "utils/msrunhandler.h"
#include <pappsomspp/msfile/msfileaccessor.h>

using namespace pappso;

void
readMzXml(Quantificator &quantificator)
{

  mcqprmout() << "opening file " << quantificator.getMsRunId().getFileName()
              << endl;
  QFileInfo mzData(quantificator.getMsRunId().getFileName());
  if(!mzData.exists())
    {
      throw MassChroqPrmException(QObject::tr("mzData file does not exists %1")
                                    .arg(mzData.absoluteFilePath()));
    }

  MsFileAccessor accessor(mzData.absoluteFilePath(), "msfile");
  MsRunReaderSPtr msrun_reader_sp =
    accessor.getMsRunReaderSPtrByRunId("", "msrun");
  MsRunHandler msrun_handler(&quantificator);
  msrun_reader_sp.get()->readSpectrumCollection(msrun_handler);
  mcqprmout() << " quantificator.debriefing()" << endl;
  quantificator.debriefing();
  // cout << "spectrum_simple size  " << spectrum_simple.getSpectrumSize()<<
  // endl;  return spectrum;
  mcqprmout() << "closing file " << quantificator.getMsRunId().getFileName()
              << endl;
}

void
writeGeneralSheet(CalcWriterInterface &writer,
                  std::vector<Quantificator *> &msrun_quantificator_list)
{
  writer.writeSheet("general");
  PrmXic::reportGeneralHeader(writer);
  for(Quantificator *&p_msrun_quantificator : msrun_quantificator_list)
    {
      p_msrun_quantificator->reportGeneralIons(writer);
    }
}


void
writeSampleSheet(CalcWriterInterface &writer,
                 std::vector<Quantificator *> &msrun_quantificator_list)
{
  writer.writeSheet("samples");
  writer.writeCell("sample id");
  writer.writeCell("sample file");
  writer.writeLine();
  for(Quantificator *&p_msrun_quantificator : msrun_quantificator_list)
    {
      writer.writeCell(p_msrun_quantificator->getMsRunId().getXmlId());
      writer.writeCell(p_msrun_quantificator->getMsRunId().getFileName());
      writer.writeLine();
    }
}

MasschroqPrmCli::MasschroqPrmCli(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}


void
MasschroqPrmCli::windaube_exit()
{
#if(WIN32)
//    mcqout() << "Press any key then enter to exit" << endl;
//    getchar();
#endif
}


// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
MasschroqPrmCli::run()
{
  //./src/masschroqprm -p ../doc/xml/peptide_list_bsa.xml -o peptide_bsa.ods
  /// gorgone/pappso/moulon/raw/test_PRM/bsa_prm_01.mzXML

  //./src/masschroqprm -p
  /// gorgone/pappso/moulon/users/Olivier/masschroqprm/bsa_prm01/xtp334/input_mcqprm.xml
  /// gorgone/pappso/moulon/raw/test_PRM/bsa_prm_01.mzXML
  try
    {

      qDebug() << "MasschroqPrmCli::run() begin";
      QCommandLineParser parser;

      parser.setApplicationDescription(
        QObject::tr("%1 %2 Mass Chromatogram Quantification on MS2 signal "
                    "(Parallel Reaction Monitoring).")
          .arg(SOFTWARE_NAME)
          .arg(MASSCHROQPRM_VERSION));
      parser.addHelpOption();
      parser.addVersionOption();
      parser.addPositionalArgument(
        "mzXML",
        QCoreApplication::translate("main", "mzXML file(s) to quantify from"));
      QCommandLineOption peptideListOption(
        QStringList() << "p"
                      << "peptides",
        QCoreApplication::translate(
          "main", "Read peptide list to quantify from <file>."),
        QCoreApplication::translate("main", "file"));
      QCommandLineOption odsOption(
        QStringList() << "o"
                      << "ods",
        QCoreApplication::translate("main",
                                    "Write results in an ODS file <output>."),
        QCoreApplication::translate("main", "output"));
      QCommandLineOption tsvOption(
        QStringList() << "t"
                      << "tsv",
        QCoreApplication::translate(
          "main", "Write results in TSV files contained in a <directory>."),
        QCoreApplication::translate("main", "directory"),
        QString());
      parser.addOption(peptideListOption);
      parser.addOption(odsOption);
      parser.addOption(tsvOption);

      qDebug() << "MasschroqPrmCli::run() 1";

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << "MasschroqPrmCli.Run is executing";

      const QDateTime dt_begin = QDateTime::currentDateTime();
      const QFileInfo masschroqprm_dir_path(
        QCoreApplication::applicationDirPath());
      const QStringList args = parser.positionalArguments();
      if(args.size() < 1)
        {
          throw PappsoException(QObject::tr("no mz file."));
        }

      QString peptideListFileStr = parser.value(peptideListOption);

      if(peptideListFileStr.isEmpty())
        {
          throw PappsoException(QObject::tr("Peptide list is empty."));
        }

      QuantificatorParam param;
      param._quantifyIsotope = true;
      // param._precursor_ion_mass_tolerance =
      // Precision::getDaltonInstance(1.5);  param._fragment_ion_mass_tolerance
      // = Precision::getDaltonInstance(0.02);

      PeptideList peptideList;
      peptideList.loadFromFile(QFileInfo(peptideListFileStr), param);
      qDebug() << "param._fragment_ion_mass_tolerance "
               << param._fragment_ion_mass_tolerance->toString();
      qDebug() << "param._precursor_ion_mass_tolerance "
               << param._precursor_ion_mass_tolerance->toString();
      qDebug() << "peptideList.size() " << peptideList.size();
      // exit(0);
      if(peptideList.size() == 0)
        {
          throw PappsoException(QObject::tr("peptide list is empty."));
        }

      CalcWriterInterface *p_writer = nullptr;
      QString odsFileStr            = parser.value(odsOption);
      if(!odsFileStr.isEmpty())
        {
          p_writer = new OdsDocWriter(odsFileStr);
        }
      QString tsvFileStr = parser.value(tsvOption);
      if(!tsvFileStr.isEmpty())
        {
          QDir directory(tsvFileStr);
          qDebug() << "QDir directory(tsvFileStr) " << directory.absolutePath()
                   << " " << tsvFileStr;
          p_writer = new TsvDirectoryWriter(directory);
        }

      if(p_writer != nullptr)
        {

          std::vector<Quantificator *> _msrun_quantificator_list;

          for(unsigned int i = 0; i < args.size(); i++)
            {
              QFileInfo mzxmlDataFileInfo(args.at(i));
              _msrun_quantificator_list.push_back(
                new Quantificator(MsRunId(mzxmlDataFileInfo.absoluteFilePath()),
                                  peptideList,
                                  param));
            }
          for(Quantificator *p_msrun_quantificator : _msrun_quantificator_list)
            {
              readMzXml(*p_msrun_quantificator);
            }
          writeGeneralSheet(*p_writer, _msrun_quantificator_list);
          writeSampleSheet(*p_writer, _msrun_quantificator_list);
          for(Quantificator *p_msrun_quantificator : _msrun_quantificator_list)
            {
              p_msrun_quantificator->report(*p_writer);
            }
          p_writer->close();
          delete p_writer;
        }
      // readMzXml();
      qDebug() << "MasschroqPrmCli::run() end";
    }

  catch(pappso::PappsoException &error)
    {
      mcqprmerr() << "Oops! an error occurred in " << QString(SOFTWARE_NAME)
                  << ". Dont Panic :" << endl;
      mcqprmerr() << error.qwhat() << endl;
      mcqprmerr() << "Try 'masschroqprm --help' for more information." << endl;
      exit(1);
      app->exit(1);
    }

  catch(std::exception &error)
    {
      mcqprmerr() << "Oops! an error occurred in " << QString(SOFTWARE_NAME)
                  << ". Dont Panic :" << endl;
      mcqprmerr() << error.what() << endl;
      mcqprmerr() << "Try 'masschroqprm --help' for more information." << endl;
      exit(1);
      app->exit(1);
    }
  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
MasschroqPrmCli::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
MasschroqPrmCli::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{

  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug() << "main begin";
  ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));

  QCoreApplication app(argc, argv);
  qDebug() << "main 1";
  QCoreApplication::setApplicationName(SOFTWARE);
  QCoreApplication::setApplicationVersion(MASSCHROQPRM_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  MasschroqPrmCli myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(
    &app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));
  qDebug() << "main 2";


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
