
/*******************************************************************************
* Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
*
* This file is part of MassChroqPRM.
*
*     MassChroqPRM is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MassChroqPRM is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
******************************************************************************/

#include "prmpreparemzcli.h"
#include "utils/mapods.h"
#include <pappsomspp/protein/enzyme.h>

#include <QCoreApplication>
#include <QTimer>
#include <QDateTime>
#include <QDir>
#include <QCommandLineParser>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/amino_acid/aa.h>
#include <pappsomspp/protein/peptidebuilder.h>
#include <pappsomspp/protein/peptidesizefilter.h>
#include <pappsomspp/protein/peptidemodificatortee.h>
#include <pappsomspp/protein/peptidefixedmodificationbuilder.h>
#include <pappsomspp/protein/peptidevariablemodificationbuilder.h>
#include <pappsomspp/protein/peptidemodificatorpipeline.h>

#include <odsstream/odsdocreader.h>
#include <odsstream/odsdocwriter.h>
#include <odsstream/tsvdirectorywriter.h>


namespace pappso {
class DigestionHandler: public EnzymeProductInterface {
public:
    void setPeptide(std::int8_t sequence_database_id, const ProteinSp & protein_sp, bool is_decoy, const QString& peptide, unsigned int start, bool is_nter, unsigned int missed_cleavage_number, bool semi_enzyme) override {
        qDebug() << start << " " << peptide;
    };
};


class PeptideModWriter: public PeptideModificatorInterface {
public:
    PeptideModWriter(CalcWriterInterface * p_writer): _p_writer(p_writer) {};

    void setPeptideSp(std::int8_t sequence_database_id, const ProteinSp & protein_sp, bool is_decoy, const PeptideSp & peptide_sp, unsigned int start, bool is_nter, unsigned int missed_cleavage_number, bool semi_enzyme) override {
        qDebug() << start << " " << peptide_sp.get()->toString();
        for (unsigned int z=1; z < 4; z++) {
            _p_writer->writeLine();
            _p_writer->writeCell(protein_sp.get()->getAccession());
            _p_writer->writeCell(peptide_sp.get()->toString());
            _p_writer->writeCell(peptide_sp.get()->getSequence());
            _p_writer->writeCell(peptide_sp.get()->getMass());
            _p_writer->writeCell((quint8) z);
            _p_writer->writeCell(peptide_sp.get()->getMz(z));
        }
    };

protected:
    CalcWriterInterface * _p_writer;
};
//AaModificationP carbamido = AaModification::getInstance("MOD:00397");
}




void generateMzList(pappso::PeptideModificatorPipeline & pipeline, MapOds & map_ods) {

    //peptide modification pipeline :
    pappso::PeptideModificatorTee mod_tee;

    // var MOD:00719@M,MOD:00696(0-1)@[YST]
    //fix MOD:00397@C
    
    //label1 MOD:00429@^K
    //label2 MOD:00552@^K
    //label3 MOD:00638@^K



    std::map< QString, std::vector< QString > > map = map_ods.getMap();
    std::vector< QString > sequence_list = map.at("sequence");

    std::vector< QString > peptide_id_list = map.at("peptide_id");

    pappso::Enzyme enzyme("([a])([a])");
    enzyme.setMiscleavage(0);

    for (unsigned int i=0; i < sequence_list.size(); i++) {
        if (!sequence_list.at(i).isEmpty()) {
            qDebug() << " generateMzList sequence = " <<  sequence_list.at(i)<< " i=" << i;
            pappso::ProteinSp protein_sp = pappso::Protein(peptide_id_list.at(i), sequence_list.at(i)).makeProteinSp();

            enzyme.eat(0,protein_sp, false,pipeline);

        }
    }

}

PrmPrepareMzCli::PrmPrepareMzCli(QObject *parent) :
    QObject(parent)
{
    // get the instance of the main application
    app = QCoreApplication::instance();
    // setup everything here
    // create any global objects
    // setup debug and warning mode
}


// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void PrmPrepareMzCli::run()
{
    //./src/prmpreparemz -i odsfile
    try {

        qDebug() << "PrmPrepareMzCli::run() begin";
        QCommandLineParser parser;

        parser.setApplicationDescription(QObject::tr("%1 %2 Mass Chromatogram Quantification on MS2 signal (Parallel Reaction Monitoring).").arg(SOFTWARE_NAME).arg(MASSCHROQPRM_VERSION));
        parser.addHelpOption();
        parser.addVersionOption();

        QCommandLineOption odsInputOption(QStringList() << "i" << "peptides",
                                          QCoreApplication::translate("main", "peptide table with peptide_id, sequence, and rt (optional) <ods>."),
                                          QCoreApplication::translate("main", "ods"));
        QCommandLineOption odsOption(QStringList() << "o" << "ods",
                                     QCoreApplication::translate("main", "Write results in an ODS file <output>."),
                                     QCoreApplication::translate("main", "output"));
        QCommandLineOption potential_mod_string_option(QStringList() << "potential-modifications",
                QCoreApplication::translate("main", "description of modifications <modification_string>."),
                QCoreApplication::translate("main", "modification_string"));
        QCommandLineOption fixed_mod_string_option(QStringList() << "modifications",
                QCoreApplication::translate("main", "description of modifications <modification_string>."),
                QCoreApplication::translate("main", "modification_string"));
        QCommandLineOption fixed_cter_mod_string_option(QStringList() << "protein-cter-modifications",
                QCoreApplication::translate("main", "description of modifications <modification_string>."),
                QCoreApplication::translate("main", "modification_string"));
        QCommandLineOption fixed_nter_mod_string_option(QStringList() << "protein-nter-modifications",
                QCoreApplication::translate("main", "description of modifications <modification_string>."),
                QCoreApplication::translate("main", "modification_string"));

        QCommandLineOption potential_cter_mod_string_option(QStringList() << "potential-protein-cter-modifications",
                QCoreApplication::translate("main", "description of modifications <modification_string>."),
                QCoreApplication::translate("main", "modification_string"));
        QCommandLineOption potential_nter_mod_string_option(QStringList() << "potential-protein-nter-modifications",
                QCoreApplication::translate("main", "description of modifications <modification_string>."),
                QCoreApplication::translate("main", "modification_string"));


        QCommandLineOption label_1_mod_string_option(QStringList() << "label-1-modifications",
                QCoreApplication::translate("main", "description of modifications <modification_string>."),
                QCoreApplication::translate("main", "modification_string"));
        QCommandLineOption label_2_mod_string_option(QStringList() << "label-2-modifications",
                QCoreApplication::translate("main", "description of modifications <modification_string>."),
                QCoreApplication::translate("main", "modification_string"));
        QCommandLineOption label_3_mod_string_option(QStringList() << "label-3-modifications",
                QCoreApplication::translate("main", "description of modifications <modification_string>."),
                QCoreApplication::translate("main", "modification_string"));
        QCommandLineOption label_4_mod_string_option(QStringList() << "label-4-modifications",
                QCoreApplication::translate("main", "description of modifications <modification_string>."),
                QCoreApplication::translate("main", "modification_string"));
        //parser.addPositionalArgument("mzXML", QCoreApplication::translate("main", "mzXML file to quantify from"));
        parser.addOption(odsInputOption);
        parser.addOption(odsOption);
        parser.addOption(fixed_mod_string_option);
        parser.addOption(fixed_cter_mod_string_option);
        parser.addOption(fixed_nter_mod_string_option);
        parser.addOption(potential_mod_string_option);
        parser.addOption(potential_cter_mod_string_option);
        parser.addOption(potential_nter_mod_string_option);

        parser.addOption(label_1_mod_string_option);
        parser.addOption(label_2_mod_string_option);
        parser.addOption(label_3_mod_string_option);
        parser.addOption(label_4_mod_string_option);




        qDebug() << "PrmPrepareMzCli::run() 1";

        // Process the actual command line arguments given by the user
        parser.process(*app);

        // QCoreApplication * app(this);
        // Add your main code here
        qDebug() << "PrmPrepareMzCli.Run is executing";

        const QDateTime dt_begin = QDateTime::currentDateTime();
        const QFileInfo masschroqprm_dir_path(QCoreApplication::applicationDirPath());


        QString odsInputFileStr = parser.value(odsInputOption);
        if (odsInputFileStr.isEmpty()) {
            throw pappso::PappsoException(QObject::tr("peptide table is needed (ODS file) with option -i"));
        }

        QFile odsFile(odsInputFileStr);
        odsFile.open(QIODevice::ReadOnly);
        MapOds map_ods;
        OdsDocReader ods_reader(map_ods);
        qDebug() << "ods_reader.parse(&odsFile)";
        ods_reader.parse(&odsFile);




        QString odsFileStr = parser.value(odsOption);
        if (odsFileStr.isEmpty()) {
            throw pappso::PappsoException(QObject::tr("ODS output file name needed with option -o"));
        }
        else {
            QFile file(odsFileStr);
            //file.open(QIODevice::WriteOnly);
            OdsDocWriter writer(&file);
            //quantificator.report(writer);
	    pappso::PeptideModWriter peptide_writer(&writer);

            pappso::PeptideModificatorPipeline pipeline;
	    pipeline.setSink(&peptide_writer);

            QString fixed_mod_string = parser.value(fixed_mod_string_option);
            if (!fixed_mod_string.isEmpty()) {
                pipeline.addFixedModificationString(fixed_mod_string);
            }
            QString fixed_cter_mod_string = parser.value(fixed_cter_mod_string_option);
            if (!fixed_cter_mod_string.isEmpty()) {
                pipeline.addFixedCterModificationString(fixed_cter_mod_string);
            }
            QString fixed_nter_mod_string = parser.value(fixed_nter_mod_string_option);
            if (!fixed_nter_mod_string.isEmpty()) {
                pipeline.addFixedNterModificationString(fixed_nter_mod_string);
            }

            QString potential_mod_string = parser.value(potential_mod_string_option);
            if (!potential_mod_string.isEmpty()) {
                pipeline.addPotentialModificationString(potential_mod_string);
            }
            QString potential_cter_mod_string = parser.value(potential_cter_mod_string_option);
            if (!potential_cter_mod_string.isEmpty()) {
                pipeline.addPotentialCterModificationString(potential_cter_mod_string);
            }
            QString potential_nter_mod_string = parser.value(potential_nter_mod_string_option);
            if (!potential_nter_mod_string.isEmpty()) {
                pipeline.addPotentialNterModificationString(potential_nter_mod_string);
            }
            QString label_1_mod_string = parser.value(label_1_mod_string_option);
            if (!label_1_mod_string.isEmpty()) {
                pipeline.addLabeledModificationString(label_1_mod_string);
            }

            QString label_2_mod_string = parser.value(label_2_mod_string_option);
            if (!label_2_mod_string.isEmpty()) {
                pipeline.addLabeledModificationString(label_2_mod_string);
            }

            QString label_3_mod_string = parser.value(label_3_mod_string_option);
            if (!label_3_mod_string.isEmpty()) {
                pipeline.addLabeledModificationString(label_3_mod_string);
            }

            QString label_4_mod_string = parser.value(label_4_mod_string_option);
            if (!label_4_mod_string.isEmpty()) {
                pipeline.addLabeledModificationString(label_4_mod_string);
            }

            generateMzList(pipeline, map_ods);
            writer.close();
            file.close();
        }

        // readMzXml();
        qDebug() << "PrmPrepareMzCli::run() end";
    }

    catch (pappso::PappsoException& error)
    {
        mcqprmerr() << "Oops! an error occurred in " << QString(SOFTWARE_NAME) << ". Dont Panic :" << endl;
        mcqprmerr() << error.qwhat() << endl;
        mcqprmerr() << "Try 'prmpreparemz --help' for more information." << endl;
        exit(1);
        app->exit(1);
    }

    catch (std::exception& error)
    {
        mcqprmerr() << "Oops! an error occurred in " << QString(SOFTWARE_NAME) << ". Dont Panic :" << endl;
        mcqprmerr() << error.what() << endl;
        mcqprmerr() << "Try 'prmpreparemz --help' for more information." << endl;
        exit(1);
        app->exit(1);
    }
    // you must call quit when complete or the program will stay in the
    // messaging loop
    quit();
}

// call this routine to quit the application
void PrmPrepareMzCli::quit()
{
    // you can do some cleanup here
    // then do emit finished to signal CoreApplication to quit
    emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void PrmPrepareMzCli::aboutToQuitApp()
{
    // stop threads
    // sleep(1);   // wait for threads to stop.
    // delete any objects
}


int main(int argc, char **argv)
{

  // ./src/prmpreparemz -i /gorgone/pappso/moulon/users/Arnaud/Regul3p/mcqprm/listepourolivier8.ods -o /tmp/test.ods --modifications "MOD:00397@C" --potential-modifications "MOD:00719@M,MOD:00696(0-1)@[YST]" --label-1-modifications "MOD:00429@(^.|K)" --label-2-modifications "MOD:00552@(^.|K)" --label-3-modifications "MOD:00638@(^.|K)"

    //QTextStream consoleErr(stderr);
    //QTextStream consoleOut(stdout, QIODevice::WriteOnly);
    //ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
    //ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
    qDebug() << "main begin";
    ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
    ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));

    QCoreApplication app(argc, argv);
    qDebug() << "main 1";
    QCoreApplication::setApplicationName(SOFTWARE);
    QCoreApplication::setApplicationVersion(MASSCHROQPRM_VERSION);
    QLocale::setDefault(QLocale::system());

    // create the main class
    PrmPrepareMzCli myMain;
    // connect up the signals
    QObject::connect(&myMain, SIGNAL(finished()),
                     &app, SLOT(quit()));
    QObject::connect(&app, SIGNAL(aboutToQuit()),
                     &myMain, SLOT(aboutToQuitApp()));
    qDebug() << "main 2";


    // This code will start the messaging engine in QT and in
    // 10ms it will start the execution in the MainClass.run routine;
    QTimer::singleShot(10, &myMain, SLOT(run()));
    return app.exec();

}
