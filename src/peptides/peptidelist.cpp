
/*******************************************************************************
* Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
*
* This file is part of MassChroqPRM.
*
*     MassChroqPRM is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MassChroqPRM is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
******************************************************************************/

#include <QDebug>
#include "peptidelist.h"
#include "../utils/masschroqprmexception.h"
#include "../quantification/quantificator.h"

#include "saxpeptidelisthandler.h"

PeptideList::PeptideList()
{

}

PeptideList::~PeptideList()
{

}

void PeptideList::loadFromFile(const QFileInfo & peptideListFileInfo, QuantificatorParam & param) {
    try {

        SaxPeptideListHandler * parser = new SaxPeptideListHandler();

        QXmlSimpleReader simplereader;
        simplereader.setContentHandler(parser);
        simplereader.setErrorHandler(parser);

        qDebug() << "Read peptide list XML file '" << peptideListFileInfo.filePath() << "'";

        if (!peptideListFileInfo.exists()) {
            throw MassChroqPrmException(
                QObject::tr("error reading peptide list file %1 does not exists").arg(
                     peptideListFileInfo.absoluteFilePath()));

        }

        QFile qfile(peptideListFileInfo.absoluteFilePath());
        QXmlInputSource xmlInputSource(&qfile);

        if (simplereader.parse(xmlInputSource)) {
        } else {
            qDebug() << parser->errorString();
            // throw PappsoException(
            //    QObject::tr("error reading tandem XML result file :\n").append(
            //         parser->errorString()));
        }
        qfile.close();

        _prmPeptideList = parser->getPeptideList();
	param = parser->getParameters();
    }

    catch (pappso::PappsoException& error)
    {
        throw pappso::PappsoException(
            QObject::tr("error PappsoException loading peptide list file %1:\n").arg(peptideListFileInfo.absoluteFilePath()).append(
                error.qwhat()));
    }

    catch (std::exception& error)
    {
        throw pappso::PappsoException(
            QObject::tr("error loading peptide list file %1:\n").arg(peptideListFileInfo.absoluteFilePath()).append(
                error.what()));
    }

}
