/*
 * /*******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 * ******************************************************************************/


#ifndef PRMPEPTIDE_H
#define PRMPEPTIDE_H

#include <pappsomspp/peptide/peptide.h>

class PrmPeptide;

typedef std::shared_ptr<const PrmPeptide> PrmPeptideSp;
typedef std::shared_ptr<PrmPeptide> NoConstPrmPeptideSp;


class PrmPeptide: public pappso::Peptide {
public:
    PrmPeptide(const QString & sequence, const QString & xmlId);
    PrmPeptide(const PrmPeptide & other);
    ~PrmPeptide();

    const PrmPeptideSp makePrmPeptideSp() const;
    NoConstPrmPeptideSp makeNoConstPrmPeptideSp() const;
    const QString & getXmlId() const {
      return (_xmlId);
    }

protected:
    const QString _xmlId;
};

#endif // PRMPEPTIDE_H
