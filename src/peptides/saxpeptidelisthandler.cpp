/*
 * /*******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 * ******************************************************************************/


#include "saxpeptidelisthandler.h"

#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/amino_acid/aamodification.h>
#include <pappsomspp/mzrange.h>
#include <QDebug>


using namespace pappso;

SaxPeptideListHandler::SaxPeptideListHandler()
{

}

SaxPeptideListHandler::~SaxPeptideListHandler()
{

}


bool SaxPeptideListHandler::startElement(const QString & namespaceURI, const QString & localName,
        const QString & qName, const QXmlAttributes & attributes) {
    // qDebug() << namespaceURI << " " << localName << " " << qName ;
    _tag_stack.push_back(qName);
    bool is_ok = true;

    try {
        //startElement_group
        if (qName == "peptide")
        {
            is_ok = startElement_peptide(attributes);
        }
        else if (qName == "psimod")
        {
            is_ok = startElement_psimod(attributes);
        } else if (qName == "daltonPrecision")
        {
            is_ok = startElement_daltonPrecision(attributes);
        } else if (qName == "ppmPrecision")
        {
            is_ok = startElement_ppmPrecision(attributes);
        } else if (qName == "parentIonMassTolerance") {
            is_ok = startElement_parentIonMassTolerance(attributes);
        } else if (qName == "fragmentIonMassTolerance") {
            is_ok = startElement_fragmentIonMassTolerance(attributes);
        }
        _currentText.clear();
    }
    catch (PappsoException exception_pappso) {
        _errorStr = QObject::tr("ERROR in SaxPeptideListHandler::startElement tag %1, PAPPSO exception:\n%2").arg(qName).arg(exception_pappso.qwhat());
        return false;
    }
    catch (std::exception exception_std) {
        _errorStr = QObject::tr("ERROR in SaxPeptideListHandler::startElement tag %1, std exception:\n%2").arg(qName).arg(exception_std.what());
        return false;
    }
    return is_ok;
}

bool SaxPeptideListHandler::endElement(const QString & namespaceURI, const QString & localName,
                                       const QString & qName) {

    bool is_ok = true;
    // endElement_peptide_list
    try {
        if (qName == "peptide")
        {
            is_ok = endElement_peptide();
        } else if (qName == "parentIonMassTolerance") {
            is_ok = endElement_parentIonMassTolerance();
        } else if (qName == "fragmentIonMassTolerance") {
            is_ok = endElement_fragmentIonMassTolerance();
        }

        // end of detection_moulon
        // else if ((_tag_stack.size() > 1) &&
        //         (_tag_stack[_tag_stack.size() - 2] == "detection_moulon"))
    }
    catch (PappsoException exception_pappso) {
        _errorStr = QObject::tr("ERROR in SaxPeptideListHandler::endElement tag %1, PAPPSO exception:\n%2").arg(qName).arg(exception_pappso.qwhat());
        return false;
    }
    catch (std::exception exception_std) {
        _errorStr = QObject::tr("ERROR in SaxPeptideListHandler::endElement tag %1, std exception:\n%2").arg(qName).arg(exception_std.what());
        return false;
    }

    _currentText.clear();
    _tag_stack.pop_back();

    return is_ok;
}

bool SaxPeptideListHandler::startElement_peptide(QXmlAttributes attributes) {
    // if ((_tag_stack.size() > 1) && (_tag_stack[_tag_stack.size() - 2] == "bioml")) {
qDebug() << "SaxPeptideListHandler::startElement_peptide begin";

// <peptide id="" seq="">
    PrmPeptide prmPeptide(attributes.value("seq"), attributes.value("id"));
    _prmPeptideSp = prmPeptide.makeNoConstPrmPeptideSp();
    return true;
}

bool SaxPeptideListHandler::startElement_psimod(QXmlAttributes attributes) {
    // if ((_tag_stack.size() > 1) && (_tag_stack[_tag_stack.size() - 2] == "protein")) {
//<psimod at="" acc="">
    // _current_peptide_start = attributes.value("at").toUInt();
    // _current_peptide_end = attributes.value("end").toUInt();
    pappso::AaModificationP aaMod = AaModification::getInstance( attributes.value("acc"));
    _prmPeptideSp.get()->addAaModification(aaMod, attributes.value("at").toUInt()-1);
    return true;
    // }
    // return true;
}

bool SaxPeptideListHandler::startElement_daltonPrecision(QXmlAttributes attributes) {
    PrecisionPtr precision =  PrecisionFactory::getDaltonInstance(attributes.value("value").toDouble());

    if (_in_fragment_ion_mass_tolerance) {
        _parameters._fragment_ion_mass_tolerance = precision;
    }
    if (_in_parent_ion_mass_tolerance) {
        _parameters._precursor_ion_mass_tolerance = precision;
    }
    return true;

}
bool SaxPeptideListHandler::startElement_ppmPrecision(QXmlAttributes attributes) {
    PrecisionPtr precision =  PrecisionFactory::getPpmInstance(attributes.value("value").toDouble());

    if (_in_fragment_ion_mass_tolerance) {
        _parameters._fragment_ion_mass_tolerance = precision;
    }
    if (_in_parent_ion_mass_tolerance) {
        _parameters._precursor_ion_mass_tolerance = precision;
    }
    return true;

}
bool SaxPeptideListHandler::startElement_parentIonMassTolerance(QXmlAttributes attributes) {
    _in_parent_ion_mass_tolerance = true;
    _in_fragment_ion_mass_tolerance = false;
    return true;
}
bool SaxPeptideListHandler::startElement_fragmentIonMassTolerance(QXmlAttributes attributes) {
    _in_fragment_ion_mass_tolerance = true;
    _in_parent_ion_mass_tolerance = false;
    return true;
}

bool SaxPeptideListHandler::endElement_parentIonMassTolerance() {
    _in_parent_ion_mass_tolerance = false;
    return true;
}
bool SaxPeptideListHandler::endElement_fragmentIonMassTolerance() {
    _in_fragment_ion_mass_tolerance = false;
    return true;
}


bool SaxPeptideListHandler::endElement_peptide() {
qDebug() << "SaxPeptideListHandler::endElement_peptide begin";
    _peptideList.push_back(_prmPeptideSp);
    return true;
}

bool SaxPeptideListHandler::error(const QXmlParseException &exception) {
    _errorStr = QObject::tr("Parse error at line %1, column %2:\n"
                            "%3").arg(exception.lineNumber()).arg(exception.columnNumber()).arg(
                    exception.message());

    return false;
}


bool SaxPeptideListHandler::fatalError(const QXmlParseException &exception) {
    _errorStr = QObject::tr("Parse error at line %1, column %2:\n"
                            "%3").arg(exception.lineNumber()).arg(exception.columnNumber()).arg(
                    exception.message());
    return false;
}

QString SaxPeptideListHandler::errorString() const {
    return _errorStr;
}


bool SaxPeptideListHandler::endDocument() {
    return true;
}

bool SaxPeptideListHandler::startDocument() {
    return true;
}

bool SaxPeptideListHandler::characters(const QString &str) {
    _currentText += str;
    return true;
}

std::list< PrmPeptideSp > SaxPeptideListHandler::getPeptideList() const {
    return _peptideList;
}

QuantificatorParam SaxPeptideListHandler::getParameters() const {
    return _parameters;
}
