
/*******************************************************************************
* Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
*
* This file is part of MassChroqPRM.
*
*     MassChroqPRM is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MassChroqPRM is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
******************************************************************************/

#ifndef PEPTIDELIST_H
#define PEPTIDELIST_H

#include <QFileInfo>
#include "prmpeptide.h"
class QuantificatorParam;

class PeptideList
{
public:
    PeptideList();
    ~PeptideList();
    void loadFromFile(const QFileInfo & peptideListFileInfo, QuantificatorParam & param);
    
    
    typedef std::list<PrmPeptideSp>::const_iterator const_iterator;

    const_iterator begin() const {
        return _prmPeptideList.begin();
    }

    const_iterator end() const {
        return _prmPeptideList.end();
    }
    
    
    unsigned int size() const {
        return _prmPeptideList.size();
    }


private:
    std::list<PrmPeptideSp> _prmPeptideList;
};

#endif // PEPTIDELIST_H
