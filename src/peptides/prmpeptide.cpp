/*
 * /*******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 * ******************************************************************************/


#include "prmpeptide.h"
#include <pappsomspp/peptide/peptide.h>

PrmPeptide::PrmPeptide(const QString & sequence, const QString & xmlId) : pappso::Peptide(sequence), _xmlId(xmlId)
{

}

PrmPeptide::~PrmPeptide()
{

}


PrmPeptide::PrmPeptide(const PrmPeptide & other) :  pappso::Peptide(other),  _xmlId(other._xmlId){
}

const PrmPeptideSp PrmPeptide::makePrmPeptideSp() const {
    return std::make_shared<PrmPeptide>(*this);
}

NoConstPrmPeptideSp PrmPeptide::makeNoConstPrmPeptideSp() const {
    return std::make_shared<PrmPeptide>(*this);
}
