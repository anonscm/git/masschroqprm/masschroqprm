/*
 * /*******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 * ******************************************************************************/

#ifndef SAXPEPTIDELISTHANDLER_H
#define SAXPEPTIDELISTHANDLER_H

#include <QXmlDefaultHandler>
#include "prmpeptide.h"
#include "../quantification/quantificator.h"


class SaxPeptideListHandler: public QXmlDefaultHandler
{
public:
    SaxPeptideListHandler();
    ~SaxPeptideListHandler();

    bool startElement(const QString & namespaceURI, const QString & localName,
                      const QString & qName, const QXmlAttributes & attributes);

    bool endElement(const QString & namespaceURI, const QString & localName,
                    const QString & qName);

    bool startDocument();

    bool endDocument();

    bool characters(const QString &str);

    bool fatalError(const QXmlParseException &exception);
    bool error(const QXmlParseException &exception);

    QString errorString() const;
    std::list< PrmPeptideSp > getPeptideList() const;
    QuantificatorParam getParameters() const;

private:
    /// error message during parsing
    QString _errorStr;
    std::vector<QString> _tag_stack;
 
    bool startElement_peptide(QXmlAttributes attributes);
    bool endElement_peptide();
    bool startElement_parentIonMassTolerance(QXmlAttributes attributes);
    bool endElement_parentIonMassTolerance();
    bool startElement_fragmentIonMassTolerance(QXmlAttributes attributes);
    bool endElement_fragmentIonMassTolerance();
    bool startElement_psimod(QXmlAttributes attributes);
    bool startElement_daltonPrecision(QXmlAttributes attributes);
    bool startElement_ppmPrecision(QXmlAttributes attributes);
    QString _currentText;
    
    NoConstPrmPeptideSp _prmPeptideSp;
    std::list<PrmPeptideSp> _peptideList;
    QuantificatorParam _parameters;
    bool _in_parent_ion_mass_tolerance = false;
    bool _in_fragment_ion_mass_tolerance = false;

};

#endif // SAXPEPTIDELISTHANDLER_H
