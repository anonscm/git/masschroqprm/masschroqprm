
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of MassChroqPRM.
 *
 *     MassChroqPRM is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroqPRM is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#include "msrunhandler.h"


MsRunHandler::MsRunHandler(Quantificator *p_quantificator)
{
  _p_quantificator = p_quantificator;
}

void
MsRunHandler::setQualifiedMassSpectrum(const pappso::QualifiedMassSpectrum &qspectrum)
{
  if(qspectrum.getMsLevel() == 2)
    {
      _p_quantificator->addQualifiedSpectrum(qspectrum);
    }
}

bool
MsRunHandler::needPeakList() const
{
  return true;
}
