
/*******************************************************************************
* Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
*
* This file is part of MassChroqPRM.
*
*     MassChroqPRM is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MassChroqPRM is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*     Olivier Langella <olivier.langella@u-psud.fr> - initial API and implementation
******************************************************************************/


#pragma once

#include <pappsomspp/msrun/msrunreader.h>
#include "../quantification/quantificator.h"


class MsRunHandler : public pappso::SpectrumCollectionHandlerInterface
{
public:
    MsRunHandler(Quantificator * p_quantificator);
    virtual void setQualifiedMassSpectrum(const pappso::QualifiedMassSpectrum & spectrum) override;
    virtual bool needPeakList () const override ;
    
private:
    Quantificator * _p_quantificator = nullptr;
};

