
/*******************************************************************************
* Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
*
* This file is part of PAPPSOms-tools.
*
*     PAPPSOms-tools is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     PAPPSOms-tools is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
******************************************************************************/

#include "mapods.h"
#include <QDebug>

MapOds::MapOds()
{

}

MapOds::~MapOds()
{

}
void MapOds::startSheet(const QString & sheet_name) {
    qDebug() << "MapOds::startSheet " <<sheet_name;
    _column = 0;
}
void MapOds::endSheet() {
    qDebug() << "MapOds::endSheet " ;
    _first_sheet = false;
}
void MapOds::startLine() {
    qDebug() << "MapOds::startLine " ;
}
void MapOds::endLine() {
    qDebug() << "MapOds::endLine " ;
    _first_line = false;
    _column = 0;
}
void MapOds::setCell(const OdsCell & ods_cell) {
    //qDebug() << "MapOds::setCell "  << ods_cell.toString();
    if(_first_sheet) {
       // qDebug() << "MapOds::setCell _first_sheet "  << _first_sheet;
        if (_first_line) {
            if (ods_cell.isEmpty()) {
            }
            else {
             qDebug() << "MapOds::setCell _first_line "  << _first_line << " " << ods_cell.toString();
               _map.insert(std::pair<QString, std::vector<QString>>(ods_cell.toString(), std::vector<QString>()));
                _map_column.insert(std::pair<unsigned int, QString>(_column, ods_cell.toString()));
            }
        }
        else {
	 // exit(0);
            if (_map_column.find(_column) != _map_column.end()) {
             qDebug() << "MapOds::setCell _first_line "  << _first_line << " " << ods_cell.toString();
               _map.at(_map_column.at(_column)).push_back(ods_cell.toString());
            }
        }
    }
    _column++;
   // qDebug() << "MapOds::setCell end" ;
}
void MapOds::endDocument() {
    qDebug() << "MapOds::endDocument " ;
}
