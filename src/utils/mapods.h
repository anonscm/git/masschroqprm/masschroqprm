
/*******************************************************************************
* Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
*
* This file is part of PAPPSOms-tools.
*
*     PAPPSOms-tools is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     PAPPSOms-tools is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
******************************************************************************/

#ifndef MAPODS_H
#define MAPODS_H

#include <QString>
#include <odsstream/reader/odscell.h>

#include <odsstream/odsdochandlerinterface.h>
#include <map>
#include <vector>

class MapOds: public OdsDocHandlerInterface
{
public:
    MapOds();
    ~MapOds();

    void startSheet(const QString & sheet_name) override;
    void endSheet()  override ;
    void startLine()override ;
    void endLine()  override ;
    void setCell(const OdsCell &) override;
    void endDocument() override ;
    std::map<QString, std::vector<QString>> & getMap() {
        return _map;
    };

private :
    std::map<QString, std::vector<QString>> _map;
    std::map<unsigned int, QString> _map_column;

    bool _first_sheet = true;
    bool _first_line = true;
    unsigned int _column=0;

};

#endif // MAPODS_H
